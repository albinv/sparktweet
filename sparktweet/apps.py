from django.apps import AppConfig


class TweetstormConfig(AppConfig):
    name = 'tweetstorm'
